import { PluginLoader } from "@intutable/core"
import { addMiddleware } from "@intutable/http/dist/requests"
import cors from "cors"

import { getAllowedOrigins } from "./config"

export async function init(plugins: PluginLoader){
    let origins : string | string[] = await getAllowedOrigins()
    plugins.request(addMiddleware(cors({
        origin: origins,
        credentials: true
    })))
}
