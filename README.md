# http-cors

CORS middleware for the HTTP plugin of IntuTable/XTable. If a web app tries to access core's [http endpoints](https://gitlab.com/intutable/http) directly from the browser, its address must first be whitelisted with the CORS plugin. As of now, the only whitelisted address is `http://localhost:3000`. Any real use of this plugin will require that it first be made configurable. Apps that wish to fetch the Core directly do not have to be registered.
